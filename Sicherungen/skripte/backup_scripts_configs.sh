#!/bin/bash - 
#===============================================================================
#
#          FILE: backup_scripts_configs.sh
# 
#         USAGE: ./backup_scripts_configs.sh 
# 
#   DESCRIPTION:This script saves the bashrc, vimrc and some config files for future system installations 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Bernd Brunner 
#  ORGANIZATION: 
#       CREATED: 2021-06-27 12:01:21
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

cp ~/.bashrc ~/Dokumente/Git_repos/linux_tips_tricks/Sicherungen/rc_config_files/
cp ~/.vimrc ~/Dokumente/Git_repos/linux_tips_tricks/Sicherungen/rc_config_files/
cp ~/.ssh/config ~/Dokumente/Git_repos/linux_tips_tricks/Sicherungen/rc_config_files/ 
cp ~/bin/* ~/Dokumente/Git_repos/linux_tips_tricks/Sicherungen/skripte/
cp ~/.bash_aliases ~/Dokumente/Git_repos/linux_tips_tricks/Sicherungen/rc_config_files/
