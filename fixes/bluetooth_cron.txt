If your Bluetooth is always on when you start your machine and you don't want that,
a cronjob can be run always on startup to deactive bluetooth right after booting
the system. This is how to do that (should work at least with Debian based distros):

1. Open crontab within the terminal:
crontab -e

2. Paste the following line at the end of the file:
@reboot bluetooth off

Another way to turn off bluetooth after login into user is to add the following line at the end of the file /etc/profile:
bluetooth off

Probably you must install tlp first before this command would work.
