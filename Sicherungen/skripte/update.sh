#!/bin/bash - 
#===============================================================================
#
#          FILE: update.sh
# 
#         USAGE: ./update.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Bernd Brunner, 
#  ORGANIZATION: 
#       CREATED: 2021-06-27 10:57:55
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
sudo apt update && sudo apt full-upgrade && sudo snap refresh && flatpak update && conda update --all

