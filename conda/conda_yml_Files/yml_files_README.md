# List of predefined Conda env

This is a collection of predefined conda environments. This can be set up very easily as soon as you have conda already installed. 

Simple type the following command and conda will set up the environments:
*conda env create <xxx.yml>*

You can find the following environments for linux and windows in the respective folders. Please look into the yml Files if youre interested in the specific package versions of the environments:

- R_env.yml: Basic R Environment with Jupyterlab instance
