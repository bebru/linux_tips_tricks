List available themes from flathub:

flatpak remote-ls flathub | grep org.gtk.Gtk3theme

To install a specific theme:

flatpak install flathub <themename>

This also works for flatpak icons:
flatpak remote-ls flathub | grep org.freedesktop.Platform.Icontheme