#!/bin/bash - 
#===============================================================================
#
#          FILE: pdf_compress.sh
# 
#         USAGE: ./pdf_compress.sh 
# 
#   DESCRIPTION: Takes pdf as an input file and compresses it
# 
#       OPTIONS: ---
#  REQUIREMENTS: Needs a pdf file as a input file
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Berndsy (), 
#  ORGANIZATION: 
#       CREATED: 2021-12-13 18:50:04
#      REVISION:  1.0
#===============================================================================

set -o nounset                              # Treat unset variables as an error

FILE_NAME="$(basename "$1" .pdf)"

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOUTPUTFILE="${FILE_NAME}"_compressed.pdf "$1"