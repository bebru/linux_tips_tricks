# This script automates the setup process for my debian installation. This script has to be run as sudo user and during installation of debian
# backports and non free must be enabled

# Update system after installation
apt-get update && apt-get -y upgrade

# install packages
apt-get install -y build-essential dkms linux-headers-$(uname -r) timeshift tlp nvidia-detect flatpak vim htop bash-completion gdebi

# Add quickfilter to synaptic
apt-get -y install apt-xapian-index
update-apt-xapian-index -vf


# add flathub to flatpak repos
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# install restricted extras
apt-get install -y ttf-mscorefonts-installer rar unrar libavcodec-extra gstreamer1.0-libav gstreamer1.0-plugins-ugly gstreamer1.0-vaapi

# install Microsoft Fonts Compatibility
apt-get -y install fonts-crosextra-carlito fonts-crosextra-caladea

# speed up boot time
#sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/' /etc/default/grub

# install firewall and enable it
apt-get install ufw && ufw enable

# enable feedback for password
cp /etc/sudoers /etc/sudoers.backup
sed -i 's/env_reset/env_reset,pwfeedback/' /etc/sudoers

# add repos for R programming language and install the latest version --> this must be changed for new debian releases

read -p "Do you want to install R? [y/n]: " choice

if [ "$choice" == "y" ]; then

    echo "" >> /etc/apt/sources.list
    echo "# R repository for latest R programming language" >> /etc/apt/sources.list
    echo "deb http://cloud.r-project.org/bin/linux/debian bullseye-cran40/" >> /etc/apt/sources.list
    echo "deb-src http://cloud.r-project.org/bin/linux/debian bullseye-cran40/" >> /etc/apt/sources.list

    # import key for rcran
    apt-key adv --keyserver keyserver.ubuntu.com --recv-key '95C0FAF38DB3CCAD0C080A7BDC78B2DDEABC47B7'

    echo "Repository for R added - Starting to install latest version of R"
    sleep 3
    apt-get update && apt-get install -y r-base r-base-dev

else
    echo "R was not installed!"
    sleep 2
fi


# add bash aliases to users home directory
USER=$(ls /home) # find the user name
USERDIR=/home/$USER

if [ ! -e "$USERDIR/.bash_aliases" ]; then

    echo "# personal aliases" >> $USERDIR/.bash_aliases
    echo ".bash_aliases file created"
    sleep 1

    echo "alias upd='sudo apt-get update'" >> $USERDIR/.bash_aliases
    echo "alias added: upd"
    sleep 1

    echo "alias upg='sudo apt-get full-upgrade && flatpak update'" >> $USERDIR/.bash_aliases
    echo "alias added: upg"
    sleep 1

    echo "alias clean='sudo apt-get autoremove && flatpak remove --unused'" >> $USERDIR/.bash_aliases
    echo "alias added: clean"
    sleep 1

    chown $USER $USERDIR/.bash_aliases
fi

# final reboot
echo "Your System will reboot now. Thanks for using a Berndsy Script to set up your Debian System."
for ((i=5 ; i>=0; i-- ));
do
    echo $i
    sleep 1
done

reboot
