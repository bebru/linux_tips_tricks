# system administration:
alias upd='sudo nala update && nala list --upgradeable'
alias upg='sudo nala upgrade && flatpak update'
alias flat='flatpak update'
alias clean='sudo nala autoremove && sudo nala clean && flatpak remove --unused'
alias network='vnstat'
alias install='sudo nala install'
alias remove='sudo nala remove'
#alias rsync='rsync -avh --info=progress2'
alias nala='sudo nala'
alias ncdu='ncdu --color=dark -e'
alias df='df -h'
alias nvidia-smi='watch -d -n 2 nvidia-smi'

# Uni Shortcuts:
alias bio='cd /home/bernd/Dokumente/Bioinformatik && fe'

# personal aliases:
alias tar_all_cores='tar --use-compress-program="pigz -k -9" -cvf'
alias rem_spaces="rename 'y/ /_/'"
alias fe='. ranger'
#alias untar='tar -xvf'
#alias tar='tar -zcvf'
alias progress='progress -mw'
alias blob='fallocate -l'

# aliases for trash
#alias rm='trash'
alias rml='trash-list'
alias rme='trash-empty'

# coding shortcuts:
alias master-jl='conda activate delir-project && cd /home/bernd/Dokumente/Bioinformatik/4_Semester/Masterarbeit/Code/master_analysis && git pull && jupyter lab .'
alias master='conda activate delir-project && cd /home/bernd/Dokumente/Bioinformatik/4_Semester/Masterarbeit/Code/master_analysis && git pull'
alias thesis='cd /home/bernd/Dokumente/Bioinformatik/4_Semester/Masterarbeit/masterthesis_tex && git pull'
alias udemy_ml='conda activate udemy_ml && cd /home/bernd/Dokumente/coding_lernen/udemy_ml/Kursmaterialien/Kursmaterialien && jupyter lab .'
alias sarah='conda activate sarah_project && cd $HOME/Dokumente/coding_projects/Sarah'
alias activate='source $(pwd)/venv/bin/activate'
alias conda_base='conda activate base'

# automatic screen lock:
alias screenlock_on='gsettings set org.gnome.desktop.screensaver lock-enabled true'
alias screenlock_off='gsettings set org.gnome.desktop.screensaver lock-enabled false'

# nVidia Management:
alias graphics='nvidia-detect query'
alias intel='nvidia-detect' 

# Power Management:
alias bat='sudo tlp bat'
alias ac='sudo tlp ac'
alias powerstat='sudo tlp-stat -s'

# Backup aliases
alias bioinf_backup='sudo rsync -aAXvh --info=progress2 /home/bernd/Dokumente/Bioinformatik/ /media/bernd/Sicherungen/Bioinformatik/'
#alias root_backup='sudo rsync -aAXvh --info=progress2 --delete --exclude={home/bernd/Downloads,home/bernd/Schreibtisch,home/bernd/VirtualBox\ VMs,home/bernd/.cache,drom,dev,proc,sys,tmp,run,mnt,media,lost+found,home/bernd/.local/share/Trash} / /media/bernd/Mint_Main_Sicher/root'
alias home_backup='sudo rsync -aAXvh --delete --info=progress2 --exclude={Dokumente/Bioinformatik,miniconda3,Downloads,Schreibtisch,VirtualBox\ VMs,.cache,.local/share/Trash,virtimages} /home/bernd/ /media/bernd/Sicherungen/home'
alias conda_envs_backup="conda env list | awk '{ print $1 }' | while read line; do conda env export -n $line > $line.yaml; done; rm .yaml; rm \#.yaml"
alias flat_backup='flatpak list --columns=app --app > $HOME/Dokumente/Git_repos/linux_tips_tricks/Sicherungen/flatpak_packages.txt'
alias flat_backup_restore='xargs flatpak install -y < flatpaks.txt'

# Shortcut for Sysrel wav conversion in mp3
alias sysrel1='for file in *.wav; do ffmpeg -i "$file" -acodec mp3 -q:a 4 "$file".mp3; done && for file in *.mp3; do rename "s/\.wav//" "$file"; done && mkdir mp3 && mv *.mp3 mp3/'
alias sysrel2='for file in *.wav; do sox -S "$file" "$file".flac; done && for file in *.flac; do rename "s/\.wav//" "$file"; done && mkdir flac && mv *.flac flac/'
