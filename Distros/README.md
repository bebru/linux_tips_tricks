# Setup Scripts

This section provides setup scripts for different Linux distribution, which I use to setup up mainly my virtual machines. The scripts can also be used for the initial setup of a production machine, but please be aware that there is the possibility that something might not work. The scripts are also tailored towards my needs so feel free to edit them for your own needs.