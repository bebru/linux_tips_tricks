# Fedora initial_setup.sh

## Introduction

Fedora is a linux distribution with the focus on open Source software. This is a great approach, since open source code is amazing and gives you a lot of freedom. But when it comes to day to day use of a computer, it is very important that all components of your system work the way they are supposed to. This is crucial if your using your machine to get work done and not just for the sake of having fun with linux. 

## What the script does in detail
#### **Edit dnf.conf:**
Setting the package manager to use the fastest available mirror and set the max. parallel downloads to 5.

#### **Update the sytem:**
This will automatically update your system. This is performed twice, again after the fusion repositories were added to ensure an up to date system.

#### **Enable rpm fusion repos:**
The rpm fusion Repositories are software repositories maintained by the Fedora community. This will multiply the available software within the software store and will also provide proprietary code like the NVIDIA graphics drivers.

#### **Install multimedia codecs**
This will install multimedia codecs so you can watch DVDs and Videos, either directly on your system or through the internet.

### **Add flathub to flatpak repos:**
This command will add the flathub repository for the flatpack package manger. Fedora provides there own flatpak repositories, but flathub remains the bigest repo of software for flatpak. You can check out a grafical software store via https://flathub.org/home.

---

From this part on the script is now based on my own preferences. If you don't like the code I will describe you can delete it prior to the execution of the script. Therefor the code is highlighted between comment lines so it should be easy to find which part you can delete. The script should remain fully functional.

---

#### **Install some software:**
- *gnome-tweaks* is a program to change parts of the apperance of the Desktop Environment Gnome
- *vim* is my prefered text editor for the terminal
- *htop* is a small terminal aplication to monitor the system ressources like cpu and ram usage.

The programs are not very big but if you know that you don't like or need them you can comment this part with a '#' symbol.

#### **Enable feedback for password:**
This enables the feedback in the terminal when you type in your password. Usually no indicators are shown, with this code enabled for each character of your password a '*' symbol will be shown.

#### **Add bash aliases to users home directory:**
This part will add terminal shortcuts. Instead of typing the whole command, it is possible to access them with the defined shortcut:
- upd = Will show you all new updates but will not install them
- upg = Will update your system. During the process you will have to confirm prior to the installation
- clean = Will delete unused packages and dependencies.

### **Final reboot:**
This will reboot your system and represents the final step of the setup. After that all changes are applied and you can use your Fedora system :smile:. 
  
