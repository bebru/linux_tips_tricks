#!/bin/bash

# script must be run as normal user
echo 'There will be input required from you! Please stay tuned'
sleep 2

# Download necessary files
wget -c https://download1.rstudio.org/desktop/jammy/amd64/rstudio-2022.02.3-492-amd64.deb
wget -c https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh

echo 'Now your Password is required for the next steps. Multiple programs will be installed and therefore sudo rights are required.'
sudo apt update && sudo apt -y upgrade 

sudo apt -y purge ubuntu-report popularity-contest apport apport-gtk whoopsie

sudo apt -y autoremove

sudo apt -y install flatpak exfat-fuse synaptic audacity gimp alacarte htop cmatrix pinta transmission simplescreenrecorder vlc vim trash-cli pavucontrol compizconfig-settings-manager texlive-full texstudio pigz gnome-tweaks build-essential ubuntu-restricted-extras gdebi curl lynx

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# add key and repos for programming language R
apt -y install --no-install-recommends software-properties-common dirmngr
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/" -y
sudo apt update
sudo apt -y install r-base r-base-core r-recommended r-base-dev

sudo apt install -y ./*.deb

# Install coding environments
sudo snap refresh
sudo snap install code --classic
sudo snap install pycharm-community --classic

# remove snap version of firefox and install flatpak version
sudo snap remove firefox

# install flatpak programs
flatpak install flathub org.mozilla.firefox com.github.tchx84.Flatseal com.mattjakeman.ExtensionManager -y

# final reboot
echo "Your System will reboot now. Thanks for using a Berndsy Script to set up your Ubuntu System."
for ((i=5 ; i>=0; i-- ));
do
    echo $i
    sleep 1
done

reboot



