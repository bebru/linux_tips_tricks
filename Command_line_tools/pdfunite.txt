With the commandline tool pdfunite it is very easy to combine different pdf files into one file.
Use the following syntax:

pdfunite <01_input.pdf> <02_input.pdf> <03_import.pdf> <output.pdf>

The last entry of the line represents the filename of the final document. You can combine as many pdfs as you wish
just seperate them with a space in between.