set background=dark
set number
set showcmd
set showmatch
set ignorecase
set smartcase
set incsearch
set mouse=a
set ruler

if has ("syntax")
 syntax on
endif
