#!/bin/bash - 
#===============================================================================
#
#          FILE: backup_rc_config_files.sh
#
#         USAGE: ./backup_rc_config_files.sh
#
#   DESCRIPTION:This script saves the bashrc, vimrc and some config files for future system installations
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Bernd Brunner
#  ORGANIZATION:
#       CREATED: 2021-06-27 12:01:21
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

cp ~/.bashrc ~/Dokumente/linux_tips_tricks/Sicherungen/rc_config_files/
cp ~/.vimrc ~/Dokumente/linux_tips_tricks/Sicherungen/rc_config_files/
cp ~/.ssh/config ~/Dokumente/linux_tips_tricks/Sicherungen/rc_config_files/
