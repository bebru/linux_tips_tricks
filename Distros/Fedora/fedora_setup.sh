#!/bin/bash

# This script must be run as sudo user

# edit dnfconf for faster downloads
echo "fastesmirror=True
max_parallel_downloads=3
defaultyes=True" >> /etc/dnf/dnf.conf

# update the system
dnf update -y

# enable rpm fusion repos
dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# update again after adding repositories
dnf update -y

# install multimedia codecs
dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
dnf install -y lame\* --exclude=lame-devel

# add flathub to flatpak repos
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

read -p "Do you want to procceed with the set up of personal preferences of Bernd Brunner? [y/n]: " choice

if [ "$choice" == "y" ]; then
#---------------------------Personal Preferences START----------------------------------
    # install some software
    echo "Installing personal software choices"
    sleep 3
    dnf install -y gnome-tweaks vim htop ffmpeg timeshift pycharm-community papirus-icon-theme smplayer tlp powertop dconf-editor vnstat
    systemctl enable vnstat.service
    dnf group install -y "C Development Tools and Libraries" "Development Tools"

    echo "Installing Webapp Manager and Yumex-dnf"    
    sleep 3
    dnf copr enable -y refi64/webapp-manager
    dnf install -y webapp-manager
    dnf copr enable -y timlau/yumex-dnf
    dnf install -y yumex-dnf

    echo "Enable feedback for password"
    cp /etc/sudoers /etc/sudoers.backup
    sed -i 's/Defaults    env_reset/Defaults    env_reset,pwfeedback/' /etc/sudoers
    sleep 3

    echo "Add bash aliases to users home directory"
    USER=$(ls /home) # find the user name
    USERDIR=/home/$USER

    mkdir -p $USERDIR/.bashrc.d

    echo "# personal aliases" >> $USERDIR/.bashrc.d/bash_aliases
    echo ".bash_aliases file created"
    sleep 1

    echo "alias upd='sudo dnf check-upgrade'" >> $USERDIR/.bashrc.d/bash_aliases
    echo "alias added: upd"
    sleep 1

    echo "alias upg='sudo dnf update'" >> $USERDIR/.bashrc.d/bash_aliases
    echo "alias added: upg"
    sleep 1

    echo "alias clean='sudo dnf autoremove'" >> $USERDIR/.bashrc.d/bash_aliases
    echo "alias added: clean"
    sleep 1

    chown -R $USER $USERDIR/.bashrc.d
    sleep 1


#--------------------------Personal Preferences END-----------------------------------
else
    echo "Personal preferences not adopted!"
    sleep 2
fi

# final reboot
echo "Your System will reboot now. Thanks for using a Berndsy Script to set up your Fedora System."
for ((i=10 ; i>=0; i-- ));
do
    echo $i
    sleep 1
done

reboot
