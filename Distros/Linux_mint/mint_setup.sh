#!/bin/bash

# to run this script as root via sudo bash setup.sh

# backup old repos and switch to faster repos
mv /etc/apt/sources.list.d/official-package-repositories.list.backup
touch /etc/apt/sources.list.d/official-package-repositories.list
cat <<EOT >> /etc/apt/sources.list.d/official-package-repositories.list
deb http://linux-mint.froonix.org una main upstream import backport 

deb http://ubuntu.lagis.at/ubuntu focal main restricted universe multiverse
deb http://ubuntu.lagis.at/ubuntu focal-updates main restricted universe multiverse
deb http://ubuntu.lagis.at/ubuntu focal-backports main restricted universe multiverse

deb http://security.ubuntu.com/ubuntu/ focal-security main restricted universe multiverse
deb http://archive.canonical.com/ubuntu/ focal partner
EOT

# Update the system
apt update && apt full-upgrade -y
apt autoremove -y

# install apt programs 
apt install htop cmatrix simplescreenrecorder vlc vim trash-cli texlive-full texstudio pigz build-essential ubuntu-restricted-extras curl lynx git ranger rename -y

# add repo for r language, import needed keys and install it
add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
apt update
apt install r-base r-base-core r-recommended r-base-dev -y

# add favourite linux mint backgrounds
apt install mint-backgrounds-tina mint-backgrounds-sonya

# install flatpak applications
flatpak install com.bitwarden.desktop com.github.tchx84.Flatseal com.github.unrud.VideoDownloader org.signal.Signal org.zotero.Zotero us.zoom.Zoom

# add bash aliases to users home directory
USER=$(ls /home) # find the user name
USERDIR=/home/$USER

echo "# personal aliases" >> $USERDIR/.bash_aliases
echo ".bash_aliases file created"
sleep 1

echo "alias upg='sudo apt update && sudo apt upgrade && flatpak update'" >> $USERDIR/.bash_aliases
echo "alias added: upg"
sleep 1

echo "alias clean='sudo apt autoremove && flatpak remove --unused'" >> $USERDIR/.bash_aliases
echo "alias added: clean"
sleep 1

echo "alias tar_all_cores='tar --use-compress-program="pigz -k -9" -cvf'" >> $USERDIR/.bash_aliases
echo "alias added: tar_all_cores"
sleep 1

echo "alias rem_spaces="rename 'y/ /_/'"" >> $USERDIR/.bash_aliases
echo "alias added: rem_spaces"
sleep 1

echo "alias fe='. ranger'" >> $USERDIR/.bash_aliases
echo "alias added: fe"
sleep 1

chown $USER $USERDIR/.bash_aliases

# install ulauncher and install colour theme:
add-apt-repository ppa:agornostal/ulauncher -y && apt update && apt install ulauncher -y
mkdir -p $USERDIR/.config/ulauncher/user-themes
git clone https://github.com/Raayib/Yaru-Dark-ulauncher.git $USERDIR/.config/ulauncher/user-themes/Yaru-Dark-ulauncher

chown -R $USER $USERDIR/.config

# final reboot
echo "Your System will reboot now. Thanks for using a Berndsy Script to set up your Linux Mint System."
for ((i=5 ; i>=0; i-- ));
do
    echo $i
    sleep 1
done

reboot
