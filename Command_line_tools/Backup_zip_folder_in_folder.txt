If you want to backup folders within a folder, but do not do compress the whole folder, to get later acces to subdirectories with this oneliner you can zip:

for i in *; do zip -r "$i.zip" "$i" && rm "$i"; done

To use all cores for this task the folling one liner would be better suited but note that tar formate is not compatible with windows:
for i in *; tar -c --use-compress-program=pigz -f $i.tar $i && rm $i; done

you can also compress the archive by using flags for pigz:
for i in *; tar -c --use-compress-program="pigz -k -9" -f $i.tar $i && rm $i; done